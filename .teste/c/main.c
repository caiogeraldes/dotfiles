#include <stdio.h>


int main () {
    int const i = 35;
    int j;
    if (i > 25) {
        j = i - 25;
    } else {
        j = i;
    }

    printf("%d\n", j);
}
